/*
 This file is part of Cubic Line Groups and Pods which is released under The MIT License.
 See file LICENSE or go to http://opensource.org/licenses/MIT for full license details.
 
 Author: Ruben Zilibowitz
 Email: ruben DOT zilibowitz AT gmail DOT com
 Date: 2nd August 2014
 */

#include <iostream>
#include <ginac/ginac.h>
#include <cassert>

using namespace GiNaC;
using namespace std;

inline ex sqr(ex e) {
    return e*e;
}

inline ex cancel_sqrt(ex e) {
	return e.subs(sqrt(sqr(wild())) == wild());
}

inline ex quadratic_solve(ex e, symbol x) {
	assert(e.degree(x) == 2);
	
	ex a = e.coeff(x,2);
	ex b = e.coeff(x,1);
	ex c = e.coeff(x,0);
	
	ex D = b*b - 4*a*c;
	
	ex r = (-b + sqrt(D)) / (2*a);
	ex s = (-b - sqrt(D)) / (2*a);
	
	lst roots = lst(r,s);
	
	return roots;
}

inline ex reduce_fractional_linear_numerator_is_square(ex f, symbol s) {
	ex fg = f.numer_denom();
	ex n = fg[0], d = fg[1];
    
    if (n.degree(s) == 1 && d.degree(s) == 1) {
        return (n/d);
    }
    
	assert(n.degree(s) == 2);
	assert(d.degree(s) == 2);
	
	ex na = n.coeff(s,2);
	ex nb = n.coeff(s,1);
	ex nc = n.coeff(s,0);
	ex discr = (nb*nb - 4*na*nc).expand();
	assert(discr.is_zero());
	
	ex n_reduced = na*(s + nb/(2*na));
	
	ex da = d.coeff(s,2);
	ex db = d.coeff(s,1);
	ex dc = d.coeff(s,0);
	
    assert(d.subs(s == -nb/(2*na)).normal().is_zero());
    
	ex d_reduced = da*(s + 2*na*dc/(da*nb));
	
	return (n_reduced / d_reduced);
}

inline ex reduce_fractional_linear_denominator_is_square(ex f, symbol s) {
    return 1/(reduce_fractional_linear_numerator_is_square(1/f,s));
}

/*
inline ex reduce_fractional_linear_numerator_has_trivial_root(ex f, symbol s) {
	ex fg = f.numer_denom();
	ex n = fg[0], d = fg[1];
	
	assert(n.degree(s) == 2);
	assert(d.degree(s) == 2);
	
	ex na = n.coeff(s,2);
	ex nb = n.coeff(s,1);
	ex nc = n.coeff(s,0);
	assert(nc.is_zero());
	
	ex n_reduced = na*s;
	
	ex da = d.coeff(s,2);
	ex db = d.coeff(s,1);
	ex dc = d.coeff(s,0);
	
    assert(d.subs(s == -nb/na).normal().is_zero());
    assert(d.subs(s == dc/da/(-nb/na)).normal().is_zero());
    
	ex d_reduced = da*(s + dc/da/(nb/na));
    
	return (n_reduced / d_reduced).normal();
}
 */

inline ex invert_fractional_linear(ex f, symbol m) {
	symbol g("g");
	ex nd = f.numer_denom();
	ex n = nd[0], d = nd[1];
	assert(n.degree(m) <= 1);
	assert(d.degree(m) <= 1);
    assert(! (n.degree(m) == 0 && d.degree(m) == 0) );
	ex result = lsolve(n == d*g, m).subs(g == m);
	return result;
}

class Cubic {
public:
	Cubic(ex aX3, ex aX2Y, ex aXY2, ex aY3, ex aX2, ex aXY, ex aY2, ex aX, ex aY, ex aC)
	: X3(aX3), X2Y(aX2Y), XY2(aXY2), Y3(aY3), X2(aX2), XY(aXY), Y2(aY2), X(aX), Y(aY), C(aC) {}
	virtual ~Cubic() {}
	
	ex unsymmetrized_projective_expression(symbol x = symbol("x"), symbol y = symbol("y"), symbol z = symbol("z"), symbol p = symbol("p"), symbol q = symbol("q"), symbol r = symbol("r")) const {
		idx ip = idx(p, 3), iq = idx(q, 3), ir = idx(r, 3);
		ex xp = indexed(x,ip), yp = indexed(y,ip), zp = indexed(z,ip);
		ex xq = indexed(x,iq), yq = indexed(y,iq), zq = indexed(z,iq);
		ex xr = indexed(x,ir), yr = indexed(y,ir), zr = indexed(z,ir);
		return (X3*xp*xq*xr +
				X2Y*(xp*xq*yr + xp*xr*yq + xr*xq*yp)/3 +
				XY2*(xp*yq*yr + xq*yp*yr + xr*yq*yp)/3 +
				Y3*yp*yq*yr +
				X2*(xp*xq*zr + xp*xr*zq + xr*xq*zp)/3 +
				XY*(xp*yq*zr + xp*yr*zq + xq*yp*zr + xq*yr*zp + xr*yp*zq + xr*yq*zp)/6 +
				Y2*(yp*yq*zr + yp*yr*zq + yr*yq*zp)/3 +
				X*(xp*zq*zr + xq*zp*zr + xr*zq*zp)/3 +
				Y*(yp*zq*zr + yq*zp*zr + yr*zq*zp)/3 +
				C*zp*zq*zr);
	}
	
	ex projective_expression(symbol x = symbol("x"), symbol y = symbol("y"), symbol z = symbol("z")) const {
		symbol p = symbol("p"), q = symbol("q"), r = symbol("r");
		ex trilinear_form = unsymmetrized_projective_expression(x,y,z,p,q,r);
		return (symmetrize(trilinear_form).subs(lst(
			indexed(x,idx(p,3))==x,indexed(x,idx(q,3))==x,indexed(x,idx(r,3))==x,
			indexed(y,idx(p,3))==y,indexed(y,idx(q,3))==y,indexed(y,idx(r,3))==y,
			indexed(z,idx(p,3))==z,indexed(z,idx(q,3))==z,indexed(z,idx(r,3))==z
		)));
	}
	
	ex affine_expression(symbol x = symbol("x"), symbol y = symbol("y")) const {
		symbol z("z");
		return projective_expression(x,y,z).subs(z == 1);
	}
	
	ex general_line_pod_equation(symbol u = symbol("u"), symbol v = symbol("v"), symbol w = symbol("w"), ex Px = symbol("Px"), ex Py = symbol("Py"), ex Qx = symbol("Qx"), ex Qy = symbol("Qy")) const {
		symbol x("x"), y("y"), z("z");
		symbol p("p"), q("q"), r("r");
		ex trilinear_form = unsymmetrized_projective_expression(x,y,z,p,q,r);
		return (trilinear_form.subs(lst(
			indexed(x,idx(p,3)) == (Px*u+Qx),indexed(x,idx(q,3)) == (Px*v+Qx),indexed(x,idx(r,3)) == (Px*w+Qx),
			indexed(y,idx(p,3)) == (Py*u+Qy),indexed(y,idx(q,3)) == (Py*v+Qy),indexed(y,idx(r,3)) == (Py*w+Qy),
			indexed(z,idx(p,3)) == 1,indexed(z,idx(q,3)) == 1,indexed(z,idx(r,3)) == 1
		)));
	}
	
	ex general_line_pod_function(symbol u = symbol("u"), symbol v = symbol("v"), ex Px = symbol("Px"), ex Py = symbol("Py"), ex Qx = symbol("Qx"), ex Qy = symbol("Qy")) const {
		symbol w("w");
		ex equation(general_line_pod_equation(u,v,w,Px,Py,Qx,Qy) == 0);
		return lsolve(equation,w);
	}
	
	ex general_line_group_function(symbol u = symbol("u"), symbol v = symbol("v"), ex Px = symbol("Px"), ex Py = symbol("Py"), ex Qx = symbol("Qx"), ex Qy = symbol("Qy"), ex identity=0) const {
		ex pod_func = general_line_pod_function(u,v,Px,Py,Qx,Qy);
        ex group_func = pod_func.subs(u==identity).subs(v==pod_func).normal();
		return group_func;
	}
    
	matrix general_line_pod_matrix(symbol u = symbol("u"), ex Px = symbol("Px"), ex Py = symbol("Py"), ex Qx = symbol("Qx"), ex Qy = symbol("Qy")) const {
		symbol v("v");
		ex func = general_line_pod_function(u,v,Px,Py,Qx,Qy).numer_denom();
		ex numer = func[0];
		ex denom = func[1];
		matrix m(2,2);
		m = numer.coeff(v,1), numer.coeff(v,0),
			denom.coeff(v,1), denom.coeff(v,0);
		return m;
	}
	
	matrix general_line_group_matrix(symbol u = symbol("u"), ex Px = symbol("Px"), ex Py = symbol("Py"), ex Qx = symbol("Qx"), ex Qy = symbol("Qy"), ex identity=0) const {
		symbol v("v");
		matrix A(2,2), B(2,2);
		A = general_line_pod_matrix(u,Px,Py,Qx,Qy);
		B = A(0,0).subs(u==identity), A(0,1).subs(u==identity),
			A(1,0).subs(u==identity), A(1,1).subs(u==identity);
		return B.mul(A);
	}
	
	ex general_field_extension(ex Px = symbol("Px"), ex Py = symbol("Py"), ex Qx = symbol("Qx"), ex Qy = symbol("Qy")) const {
		symbol u("u");
		matrix m(general_line_pod_matrix(u,Px,Py,Qx,Qy));
		ex det = m.determinant();
		ex a = det.coeff(u,2);
		ex b = det.coeff(u,1);
		ex c = det.coeff(u,0);
		return (b*b - 4*a*c);
	}
	
	ex parametrisation(symbol m = symbol("m"), ex Px = symbol("Px"), ex Py = symbol("Py"), ex Qx = symbol("Qx"), ex Qy = symbol("Qy"), ex identity=0) const {
		symbol u("u");
        
        matrix A0 = general_line_pod_matrix(u,Px,Py,Qx,Qy);
//        cout << "general_line_pod_matrix " << endl << A0(0,0).normal() << " " << A0(0,1).normal() << endl << A0(1,0).normal() << " " << A0(1,1).normal() << endl;
		A0 = A0(0,0).subs(u==identity), A0(0,1).subs(u==identity),
             A0(1,0).subs(u==identity), A0(1,1).subs(u==identity);
        ex V = A0.determinant();
        
		matrix group_mat(general_line_group_matrix(u,Px,Py,Qx,Qy,identity));
//        cout << "general_line_group_matrix " << endl << group_mat(0,0).normal() << " " << group_mat(0,1).normal() << endl << group_mat(1,0).normal() << " " << group_mat(1,1).normal() << endl;
		ex det = group_mat.determinant();

		assert(V.is_polynomial(lst(Px,Py,Qx,Qy)));
		ex lhs = factor(det - sqr(m*(u-identity)+V)) / (u-identity);
        
		ex X = lsolve(lhs == 0, u);
		ex Y = m*(X-identity) + V;
		
		lst result;
		result = X, Y;
		
		return result;
	}
	
	matrix general_line_group_matrix_normalised(symbol m = symbol("m"), ex Px = symbol("Px"), ex Py = symbol("Py"), ex Qx = symbol("Qx"), ex Qy = symbol("Qy"), ex identity=0) const {
		symbol u("u");
		matrix B(general_line_group_matrix(u,Px,Py,Qx,Qy,identity));
		matrix B_normalised(2,2);
		ex param(parametrisation(m,Px,Py,Qx,Qy,identity));
		ex X(param[0]), Y(param[1]);
		B_normalised = B(0,0).subs(u==X)/Y, B(0,1).subs(u==X)/Y,
					   B(1,0).subs(u==X)/Y, B(1,1).subs(u==X)/Y;
		return B_normalised;
	}
	
	ex char_poly(symbol t = symbol("t"), symbol m = symbol("m"), ex Px = symbol("Px"), ex Py = symbol("Py"), ex Qx = symbol("Qx"), ex Qy = symbol("Qy"), ex identity=0) const {
		matrix B(general_line_group_matrix_normalised(m,Px,Py,Qx,Qy,identity));
//        cout << "general_line_group_matrix_normalised " << endl << B(0,0).normal() << " " << B(0,1).normal() << endl << B(1,0).normal() << " " << B(1,1).normal() << endl;
        return B.charpoly(t);
	}
	
	ex eigs(symbol m = symbol("m"), ex Px = symbol("Px"), ex Py = symbol("Py"), ex Qx = symbol("Qx"), ex Qy = symbol("Qy"), ex identity=0) const {
		symbol t("t");
		ex poly(char_poly(t,m,Px,Py,Qx,Qy,identity));
		ex a = poly.coeff(t,2);
		ex b = poly.coeff(t,1);
		ex c = poly.coeff(t,0);
		ex ext = general_field_extension(Px,Py,Qx,Qy);
        if (ext.normal().is_zero()) {
            assert((b*b - 4*a*c).normal().is_zero());
            return lst(-b / (2*a));
        }
        else {
            ex discriminant_over_ext = ((b*b - 4*a*c)/ext).numer_denom();
            ex top = cancel_sqrt(sqrt(sqrfree(discriminant_over_ext[0],lst(m))));
            ex bot = cancel_sqrt(sqrt(sqrfree(discriminant_over_ext[1],lst(m))));
            ex frac = top/bot;
            
            ex r = (-b + sqrt(ext)*frac) / (2*a);
            ex s = (-b - sqrt(ext)*frac) / (2*a);
            
            lst l;
            l = reduce_fractional_linear_numerator_is_square(r,m), reduce_fractional_linear_numerator_is_square(s,m);
            
            return l;
        }
	}
	
	ex iso(symbol g = symbol("g"), ex Px = symbol("Px"), ex Py = symbol("Py"), ex Qx = symbol("Qx"), ex Qy = symbol("Qy"), ex identity=0) const {
		symbol m("m");
		ex E0 = eigs(m,Px,Py,Qx,Qy,identity)[0];
		ex E0_inverse = invert_fractional_linear(E0,m).subs(m == g);
		ex X = parametrisation(m,Px,Py,Qx,Qy,identity)[0];
		ex result = X.subs(m == E0_inverse);
		return result;
	}
	
protected:
	const ex X3;	// x^3
	const ex X2Y;	// x^2*y
	const ex XY2;	// x*y^2
	const ex Y3;	// y^3
	
	const ex X2;	// x^2
	const ex XY;	// x*y
	const ex Y2;	// y^2
	
	const ex X;	// x
	const ex Y;	// y
	const ex C;	// constant term
};

int main()
{
//	numeric Qxval=2,Qyval=1;//,identity=1;
	numeric Qxval=0,Qyval=0;
	symbol Pyval("m");
    numeric identity=0, Pxval=1;
    symbol n("n");
    
// 	Cubic cubic(1,0,0,1,0,0,0,0,0,-1);	// x^3 + y^3 - 1
    Cubic cubic(-1,0,0,0,0,0,1,n*n,0,0);   // y^2 == x^3 - n^2*x    : Congruent number curve. If this has non-trivial rational points, then n is a "congruent number".
// 	Cubic cubic(0,-64,-64,0,0,48,0,0,0,-1);	// 16xy(3-4(x+y)) == 1   : Jumping jack cubic
//	Cubic cubic(-1,0,0,0,0,0,1,2,0,-1);
//	Cubic cubic(1,0,0,1,0,0,0,0,0,-9);
	
	cout << "line equation P*u + Q:" << endl;
	cout << "P = " << Pxval << "," << Pyval << "; Q = " << Qxval << "," << Qyval << "; identity u=" << identity << endl;
	
	cout << "affine cubic equation:" << endl;
	cout << cubic.affine_expression() << endl;
    
	cout << "line pod function:" << endl;
	symbol u("u"), v("v");
	const ex line_pod_function = cubic.general_line_pod_function(u,v,Pxval,Pyval,Qxval,Qyval);
	cout << line_pod_function << endl;
    
	cout << "line group function:" << endl;
	const ex line_group_function = cubic.general_line_group_function(u,v,Pxval,Pyval,Qxval,Qyval,identity);
	cout << line_group_function << endl;
    {
        cout << "inverse of u for line group:" << endl;
        ex nd = line_group_function.numer_denom();
        ex inverse_u = lsolve(nd[0]==identity*nd[1],v);
        cout << inverse_u << endl;
	}
    
	cout << "field extension of rationals:" << endl;
    ex field_extension = cubic.general_field_extension(Pxval,Pyval,Qxval,Qyval);
	cout << factor(field_extension) << endl;
    
    if (field_extension == 0) {
        cout << "isomorphisms from Rationals under addition to line pod:" << endl;
        symbol a("a"), b("b"), c("c"), d("d");
        symbol U("U"), V("V");
        ex eq = (line_group_function.subs(lst(u==(a*U+b)/(c*U+d),v==(a*V+b)/(c*V+d))) - (a*(U+V)+b)/(c*(U+V)+d)).numer();
        ex eqs = lst(factor(eq.coeff(U,0).coeff(V,0)),factor(eq.coeff(U,0).coeff(V,1)),factor(eq.coeff(U,0).coeff(V,2)),factor(eq.coeff(U,1).coeff(V,0)),factor(eq.coeff(U,1).coeff(V,1)),factor(eq.coeff(U,1).coeff(V,2)),factor(eq.coeff(U,2).coeff(V,0)),factor(eq.coeff(U,2).coeff(V,1)),factor(eq.coeff(U,2).coeff(V,2)));
        
        size_t N = eqs.nops();
        ex current_gcd = 1;
        size_t i;
        for (i = 0; i < N; i++) {
            current_gcd = eqs.op((i+1) % N);
            for (size_t j = 0; j < N; j++) {
                if (i != j) {
                    current_gcd = gcd(current_gcd,eqs.op(j));
                }
            }
            if (! (current_gcd - 1).is_zero()) {
                break;
            }
        }
        
        assert(! (current_gcd - 1).is_zero());
        assert(i < N);
        
        ex iso = (a*U+b)/(c*U+d);
        ex current_gcd_op0 = (current_gcd.nops() > 0 ? current_gcd.op(0) : current_gcd);
        
//        cout << "gcd found " << current_gcd << endl;
//        cout << "remaining factors " << eqs.op(i) << endl;
        
//        cout << "current_gcd_op0 " << current_gcd_op0 << endl;
        assert(current_gcd.degree(current_gcd_op0) == 1);
        iso = iso.subs(current_gcd_op0 == lsolve(current_gcd == 0, current_gcd_op0));
        
        for (const_iterator it = eqs.op(i).begin(); it != eqs.op(i).end(); it++) {
            ex fac;
            if (is_a<power>(*it)) {
                fac = it->op(0);
            }
            else {
                fac = *it;
            }
            ex fac_op0 = (fac.nops() > 0 ? fac.op(0) : fac);
            if ( ! is_a<numeric>(fac_op0.normal()) ) {
                assert(fac.degree(fac_op0) == 1);
                ex an_iso = iso.subs(fac_op0 == lsolve(fac == 0, fac_op0));
                if ( ! is_a<numeric>(an_iso.normal()) ) {
                    cout << an_iso << endl;
                    cout << "inverse isomorphism" << endl;
                    cout << invert_fractional_linear(an_iso,U) << endl;
                    
                    {
                        symbol x("x"), y("y");
                        cout << "verify isomorphism" << endl;
                        cout << "Does iso(x) *line group* iso(y) == iso(x+y) ? ";
                        ex isox = an_iso.subs(U==x);
                        ex isoy = an_iso.subs(U==y);
                        ex isoxpy = an_iso.subs(U==x+y);
                        cout << (line_group_function.subs(lst(u == isox, v == isoy)) - isoxpy).normal().is_zero() << endl;
                    }
                }
            }
        }
    }
	else {
		symbol g("g"), h("h");
		ex iso = cubic.iso(g,Pxval,Pyval,Qxval,Qyval,identity);
		
 		ex nd = iso.numer_denom();
 		ex n = nd[0], d = nd[1];
 		
 		n = n.expand().collect(g);
 		d = d.expand().collect(g);
 		
 		assert(n.coeff(g,1).is_zero());
 		assert(d.coeff(g,1).is_zero());
		
        // compute isomorphism
        iso = (n/d).subs(g*g == g).normal();
        
        if (field_extension > 0) {
            cout << "isomorphism from Rationals without zero under multiplication to line pod:" << endl;
            cout << iso << endl;
            cout << "inverse isomorphism" << endl;
            cout << invert_fractional_linear(iso,g) << endl;
            
            {
                symbol x("x"), y("y");
                cout << "verify isomorphism" << endl;
                cout << "Does iso(x) *line group* iso(y) == iso(x*y) ? ";
                ex isox = iso.subs(g==x);
                ex isoy = iso.subs(g==y);
                ex isoxpy = iso.subs(g==x*y);
                cout << (line_group_function.subs(lst(u == isox, v == isoy)) - isoxpy).normal().is_zero() << endl;
            }
        }
        else {
            cout << "isomorphism from unit circle parametrised as (1+I*h)^2/(1+h^2) under complex multiplication to line pod" << endl;
            ex circle_iso = iso.subs(g == sqr(1+I*h)/(1 + h*h)).normal();
            assert(circle_iso.imag_part().subs(lst(imag_part(h) == 0, real_part(h) == h)).normal().is_zero());
            circle_iso = (circle_iso.real_part().subs(lst(imag_part(h) == 0, real_part(h) == h))).normal();
            circle_iso = reduce_fractional_linear_denominator_is_square(circle_iso,h);
            cout << circle_iso << endl;
            cout << "inverse isomorphism" << endl;
            cout << invert_fractional_linear(circle_iso,h) << endl;
            
            {
                symbol x("x"), y("y");
                cout << "verify isomorphism" << endl;
                cout << "Does iso(x) *line group* iso(y) == iso((x+y)/(1-x*y)) ? ";
                ex isox = circle_iso.subs(h==x);
                ex isoy = circle_iso.subs(h==y);
                ex isoxpy = circle_iso.subs(h==(x+y)/(1-x*y));
                cout << (line_group_function.subs(lst(u == isox, v == isoy)) - isoxpy).normal().is_zero() << endl;
            }
        }
 	}
}
